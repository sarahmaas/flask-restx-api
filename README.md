Python Flask Boilerplate
=======


### To run locally:
* Clone this repository
    ```bash
    git clone https://sarahmaas@bitbucket.org/sarahmaas/flask-restx-api.git
    ```
* Install python 3.9 and pip
* Create base venv (virtual environment)
    ```bash
    pip install virtualenv
    virtualenv -p `which python3.9` venv
    ```
* Activate venv
    ```bash
    source venv/bin/activate
    ```
* Rebuild venv with dependencies
    ```bash
    pip install -Ur requirements.txt
    ```
* Run app (after activating venv)
    ```bash
    python rest_api/app.py
    ```

* Open swagger UI at http://127.0.0.1:5000/api/.

Alternatively, most of these steps are set up in the makefile.

## Assumptions:
* This is at best a POC demo. To run in production, it'd need a number of things, including but not limited to:
 a server like uWSGI enabled, an actual datastore.

## Notes:
### With more time I would:
* figure out why logging isn't going to console
* enable an actual db, and use a dockerized instance for local dev - for example Redis
* add automated tests, wasn't sure how to manage this with the mocked out data service - happy to talk through my usual testing strategies in discussion
* add linting/type checks and fix where it's missing
* add Dockerfile to be able to run the application in a container
* address the various todos
* better error/exception handling
* clean up some awkward naming
### In a production setting I would also expect to enable:
* an actual db with proper connections including any libraries for access
* uWSGI or gunicorn server
* dockerized deployment
* authentication
* a more robust time handling schema
* caching
* annotations on functions

## Seed Data (if desired):
This is what I used for some basic testing.

POST schedule payloads:
```bash
curl -X 'POST' \
  'http://127.0.0.1:5000/api/schedule/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "3",
  "times": ["12:05 PM"]
}'
```
```bash
curl -X 'POST' \
  'http://127.0.0.1:5000/api/schedule/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "5",
  "times": ["12:05 AM", "12:05 PM", "5:05 PM"]
}'
```
```bash
curl -X 'POST' \
  'http://127.0.0.1:5000/api/schedule/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "C",
  "times": ["5:05 PM"]
}'
```

GET schedule:
```bash
curl -X 'GET' \
  'http://127.0.0.1:5000/api/schedule/' \
  -H 'accept: application/json'
```
Expected to return:
```json
{
  "5": [
    "5"
  ],
  "1205": [
    "3",
    "5"
  ],
  "1705": [
    "5",
    "C"
  ]
}
```


Expected GET schedule/next:

* 1:00 AM ->   12:05 PM
* 1:14 PM ->    5:05 PM
* 2:00 PM ->    5:05 PM
* 6:00 PM ->   12:05 PM
