import logging
from time import strptime
from typing import Dict, List, Optional, Any

from rest_api.database import db

log = logging.getLogger(__name__)


def get_time_value(input_time: str) -> Optional[int]:
    try:
        time_st = strptime(input_time, '%I:%M %p')
        time_value = time_st.tm_hour * 100 + time_st.tm_min
        return time_value
    except ValueError:
        log.error('Bad time %s entered', input_time)


def display_time(time_value: int) -> str:
    hours = time_value // 100
    minutes = time_value - hours * 100
    am_pm = 'AM' if hours < 12 else 'PM'
    hours %= 12
    if hours == 0:
        hours = 12
    return f'{hours}:{minutes:02d} {am_pm}'


def format_times_for_storage(times: List[str]) -> List[int]:
    if not times:
        # do not operate on empty list
        return times
    values = [get_time_value(t) for t in times]
    values.sort()
    return values


def build_schedule() -> Dict[int, List[str]]:
    schedule = {}
    for train_line in db.keys():
        times = db.fetch(train_line)
        for t in times:
            schedule.setdefault(t, [])
            schedule[t].append(train_line)
    return schedule


def find_next(start: int, sorted_keys: List[int], dataset: dict) -> Optional[int]:
    # loops back to beginning if one is not found after given start
    for starting_value in [start, 0]:
        for key in sorted_keys:
            if key < starting_value:
                continue
            # has multiple
            if len(dataset[key]) > 1:
                return key
    return None


def get_next_time_from_schedule(input_time: str,
                                schedule: Optional[Dict[int, List[str]]] = None) -> Optional[str]:
    input_time_value = get_time_value(input_time)
    if not schedule:
        schedule = build_schedule()
    schedule_times = list(schedule.keys())
    schedule_times.sort()
    time_value = find_next(input_time_value, schedule_times, schedule)
    return display_time(time_value) if time_value else None


def get_schedule():
    # TODO: could be optimized to cache build schedule
    return build_schedule(), 200


def post_schedule(request_arguments: Dict[str, Any]):
    # get args from request
    name = request_arguments.get('name')
    times = request_arguments.get('times')
    if not name or not times:
        log.warning('Bad request, missing name %s or times %s', name, times)
        return "Must specify 'name' and 'times'", 400
    times_as_numbers = format_times_for_storage(times)
    db.set(name, times_as_numbers)
    return None, 200


def get_next(request_arguments: Dict[str, Any]):
    # get args from request
    input_time = request_arguments.get('time')
    log.debug('Getting next time from schedule for input_time %s', input_time)
    next_time = get_next_time_from_schedule(input_time)
    return next_time, 200
