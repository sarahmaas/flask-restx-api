from flask import request
from flask_restx import Resource

from rest_api.api.restx_api import api
from rest_api.api.schedule import models
from rest_api.api.schedule.business import get_next, post_schedule, get_schedule

ns = api.namespace('schedule', description='Operations related to train schedule')


@ns.route('/')
class TrainSchedule(Resource):
    def get(self):
        # whole schedule
        return get_schedule()

    @api.response(200, 'Schedule successfully posted.')
    @api.expect(models.schedule_request)
    def post(self):
        data = request.get_json(force=True)
        return post_schedule(data)


@ns.route('/next')
class NextTrains(Resource):
    @api.expect(models.next_parser)
    def get(self):
        args = models.next_parser.parse_args()
        return get_next(args)
