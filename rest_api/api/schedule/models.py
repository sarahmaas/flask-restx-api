from flask_restx import fields

from rest_api.api.restx_api import api

# TODO: could write a custom schema format to validate times
# TODO: update full schedule times to show formatted keys on API response

schedule_request = api.model('Schedule Request', {
    'name': fields.String(required=True),
    'times': fields.List(fields.String(required=True)),
})

next_parser = api.parser()
next_parser.add_argument('time', location='args', type=str, help='Time in format like 5:09 PM')
