import logging
from typing import Mapping

from flask import Blueprint
from flask_restx import Resource, Api

log = logging.getLogger(__name__)

blueprint = Blueprint('api', __name__, url_prefix='/api')
api = Api(blueprint, version='1.0', title='Train Schedule API',
          description="REST endpoints for a train station's schedule",
          validate=True)

ns = api.namespace('status', description='Status of the API')


@ns.route('')
class Status(Resource):
    def get(self) -> Mapping:
        return {'success': True}
