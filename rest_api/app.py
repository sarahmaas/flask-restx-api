import logging

from flask import Flask

from rest_api.api.restx_api import blueprint
from rest_api.api.schedule import service as schedule  # automatically registers

app = Flask(__name__)
app.register_blueprint(blueprint)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s')
    app.run()
