"""
This is a mock data service interface based on the requirements given
to attempt at providing a runnable API.
"""

from typing import Dict, List, Optional


class DataService:
    """Mock db service"""

    def __init__(self, initial_data: Optional[Dict[str, List[int]]] = None):
        self.data_store: Dict[int, List[int]] = initial_data or {}

    def keys(self) -> List[str]:
        return self.data_store.keys()

    def fetch(self, key: str) -> List[int]:
        return self.data_store[key]

    def set(self, key: str, values: List[int]) -> None:
        self.data_store.update({key: values})
